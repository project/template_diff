# Overview

When developing a theme, there is often a need to understand why a theme needed
to override the template provided by the base theme. Displaying the diff between
the two templates would help with this.

This module provides a command that accepts a template name and will display the
diff between two specified themes or modules. If no theme is specified it
defaults to the active theme.

## Usage

### Options

There are no custom options, but the use of `-vv` will show additional output.

### Examples

Compare the the `views-view` template in `my_theme` theme vs `your_theme`.
```
$ drush template_diff:show views-view my_theme your_theme
```

Compare the the `views-view` template in `my_theme` theme vs `your_module`.
```
$ drush template_diff:show views-view my_theme your_module
```

Compare the the `views-view` template in the active theme vs its base theme.
```
$ drush template_diff:show views-view
```

Full output.
```
$ drush template_diff:show views-view
 [notice] Comparing chromatic (active theme) and stable (base theme).
- stable
+ chromatic
@@ @@
 {#
 /**
  * @file
- * Theme override for main view template.
+ * Default theme implementation for main view template.
  *
  * Available variables:
  * - attributes: Remaining HTML attributes for the element.
- * - css_name: A CSS-safe version of the view name.
+ * - css_name: A css-safe version of the view name.
  * - css_class: The user-specified classes names, if any.
  * - header: The optional header.
  * - footer: The optional footer.
@@ @@
  *   Javascript.
  *
  * @see template_preprocess_views_view()
+ *
+ * @ingroup themeable
  */
 #}
 {%
   set classes = [
     dom_id ? 'js-view-dom-id-' ~ dom_id,
+    'structure__content-list',
+    'structure__row',
   ]
 %}
 <div{{ attributes.addClass(classes) }}>
@@ @@
   {{ exposed }}
   {{ attachment_before }}

-  {% if rows -%}
-    {{ rows }}
-  {% elseif empty -%}
-    {{ empty }}
-  {% endif %}
+  {{ rows }}
+  {{ empty }}
   {{ pager }}

   {{ attachment_after }}
```
