<?php

namespace Drupal\template_diff\Commands;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\template_diff\TemplateLocation;
use Drush\Commands\DrushCommands;
use SebastianBergmann\Diff\Differ;
use SebastianBergmann\Diff\Output\UnifiedDiffOutputBuilder;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class TemplateDiffCommands extends DrushCommands {

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface;
   */
  protected $moduleHandler;

  /**
   * Builds the template diff service.
   */
  public function __construct(
    ThemeHandlerInterface $themeHandler,
    ThemeManagerInterface $themeManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->themeHandler = $themeHandler;
    $this->themeManager = $themeManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Displays the diff between two templates files in specified themes.
   *
   * @param string $template_name
   *   The template name.
   * @param ?string $location_1
   *   The first theme name to compare.
   * @param ?string $location_2
   *   The second theme name to compare.
   *
   * @usage template_diff:show node foo_theme bar_theme
   *   Compare the node template in two themes
   *
   * @command template_diff:show
   */
  public function commandName(string $template_name, string $location_1 = NULL, string $location_2 = NULL) {
    // Add the Twig file extension if it was not included.
    if (strpos($template_name, 'html.twig') === FALSE) {
      $template_name = sprintf('%s.html.twig', $template_name);
    }
    // Determine the active theme.
    $active_theme = $this->themeManager->getActiveTheme()->getName();

    // Build template location objects.
    $template_1 = new TemplateLocation(
      $this->themeHandler,
      $this->themeManager,
      $this->moduleHandler,
      $template_name,
      $location_1
    );
    $template_2 = new TemplateLocation(
      $this->themeHandler,
      $this->themeManager,
      $this->moduleHandler,
      $template_name,
      $location_2
    );
    // Validate the location values.
    foreach ([$template_1, $template_2] as $template) {
      if (!$template->isValid()) {
        $this->logger->warning(dt('No theme or module with name :location found.', [
          ':location' => $template->getLocation(),
        ]));
        return;
      }
    }

    // Compare the two locations that were passed in as arguments.
    if (!$template_1->isEmpty() && !$template_2->isEmpty()) {
      // Log the locations being compared and how we determined them.
      $this->logger->notice(dt('Comparing :location_1 and :location_2.', [
        ':location_1' => $template_1->getLocation(),
        ':location_2' => $template_2->getLocation(),
      ]));
    }
    // If one location was supplied, use the active theme as the second one.
    elseif (!$template_1->isEmpty() && $template_2->isEmpty()) {
      $template_2->setLocation($active_theme);
      // Log the locations being compared and how we determined them.
      $this->logger->notice(dt('Comparing :location_1 and the active theme (:location_2).', [
        ':location_1' => $template_1->getLocation(),
        ':location_2' => $template_2->getLocation(),
      ]));
    }
    // Compare the active theme and its parent theme.
    else {
      // Set the active theme as the first location.
      $template_1->setLocation($active_theme);
      // Find the parent theme. If no base theme is set, the theme will
      // inherit from core's stable theme by default.
      $themes = $this->themeHandler->listInfo();
      $template_2->setLocation($themes[$template_1->getLocation()]->base_theme);
      // Log the locations being compared and how we determined them.
      $this->logger->notice(dt('Comparing :location_1 (active theme) and :location_2 (base theme).', [
        ':location_1' => $template_1->getLocation(),
        ':location_2' => $template_2->getLocation(),
      ]));
    }

    // Log where we are searching for templates.
    $this->logger->debug(dt("Searching for template files in:\n  - :location_1_path\n  - :location_2_path", [
      ':location_1_path' => $template_1->getTemplateFolderPath(),
      ':location_2_path' => $template_2->getTemplateFolderPath(),
    ]));
    // If a template was not found, log the failure and abort the command.
    foreach ([$template_1, $template_2] as $template) {
      if (!$template->getTemplateFilePath()) {
        $this->logger->warning(dt('Unable to find a :template template in the :location :type.', [
          ':template' => $template_name,
          ':location' => $template->getLocation(),
          ':type' => $template->getType(),
        ]));
        return;
      }
    }
    // Log the template and themes being compared.
    $this->logger->debug(dt("Comparing files:\n  - :path_1\n  - :path_2", [
      ':path_1' => $template_1->getTemplateFilePath(),
      ':path_2' => $template_2->getTemplateFilePath(),
    ]));

    // Build and display the diff output.
    $builder = new UnifiedDiffOutputBuilder(
      sprintf("- %s\n+ %s\n", $template_2->getLocation(), $template_1->getLocation()),
      FALSE,
    );
    $differ = new Differ($builder);
    print $differ->diff(
      file_get_contents($template_2->getTemplateFilePath()),
      file_get_contents($template_1->getTemplateFilePath())
    );
  }

}
