<?php

namespace Drupal\template_diff;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Stores and manages information about a template location.
 */
class TemplateLocation {

  /**
   * Define the location types.
   */
  const TYPE_THEME = 'theme';
  const TYPE_MODULE = 'module';

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface;
   */
  protected $moduleHandler;

  /**
   * The template name.
   *
   * @var string
   */
  protected $template;

  /**
   * The template file path.
   *
   * @var string
   */
  protected $templateFilePath = NULL;

  /**
   * An array of modules paths keyed by module name.
   *
   * @var array
   */
  protected $modules;

  /**
   * An array of themes keyed by theme name.
   *
   * @var \Drupal\Core\Extension\Extension[]
   */
  protected $themes;

  /**
   * Builds the template diff service.
   */
  public function __construct(
    ThemeHandlerInterface $themeHandler,
    ThemeManagerInterface $themeManager,
    ModuleHandlerInterface $moduleHandler,
    string $template,
    string $location = NULL
  ) {
    $this->themeHandler = $themeHandler;
    $this->themeManager = $themeManager;
    $this->moduleHandler = $moduleHandler;
    $this->template = $template;
    $this->location = $location;
    $this->modules = $this->moduleHandler->getModuleDirectories();
    $this->themes = $this->themeHandler->listInfo();
  }

  /**
   * Indicates if any location information was set.
   *
   * @return bool
   *   True if no location was found.
   */
  public function isEmpty() {
    return empty($this->location);
  }

  /**
   * Get the location.
   *
   * @return string
   */
  public function getLocation() {
    return $this->location;
  }

  /**
   * Set the location.
   *
   * @param string|null $location
   *   The location information (theme or module name).
   */
  public function setLocation(string $location = NULL) {
    $this->location = $location;
  }

  /**
   * Get the path to the location.
   *
   * @return false|string
   *   The module or theme path.
   */
  public function getPath() {
    if ($this->getType() == self::TYPE_MODULE) {
      return $this->moduleHandler->getModule($this->location)->getPath();
    }
    if ($this->getType() == self::TYPE_THEME) {
      return $this->themeHandler->getTheme($this->location)->getPath();
    }
    return FALSE;
  }

  /**
   * Build the path to the template folder.
   *
   * @return string
   *   The location path with the "templates" folder appended to the path.
   */
  public function getTemplateFolderPath() {
    return sprintf('%s/templates', $this->getPath());
  }

  /**
   * Get the path to the template file.
   *
   * @return false|string
   *   The file path if a file was found.
   */
  public function getTemplateFilePath() {
    if (!empty($this->templateFilePath)) {
      return $this->templateFilePath;
    }
    return $this->findTemplateFilePath();
  }

  /**
   * Validate the location.
   *
   * @return bool
   *   TRUE if the location is empty or if it is a valid module or theme.
   */
  public function isValid() {
    // An empty location is valid.
    if ($this->isEmpty()) {
      return TRUE;
    }
    // If the location is not empty, confirm that it is a valid module or theme.
    if ($this->getType()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the location type by validating if it is a module or theme.
   *
   * @return false|string
   *   The type string or false if the location is not valid.
   */
  public function getType() {
    if (isset($this->themes[$this->location])) {
      return self::TYPE_THEME;
    }
    if (isset($this->modules[$this->location])) {
      return self::TYPE_MODULE;
    }
    return FALSE;
  }

  /**
   * Find the template file path.
   *
   * @return false|string
   *   The template file path if one was found, otherwise false.
   */
  private function findTemplateFilePath() {
    // Search for files in each theme that match the specified template.
    $finder = new Finder();
    $finder_files = $finder->in($this->getTemplateFolderPath())
      ->name($this->template)
      ->files();
    if (empty($finder_files)) {
      return FALSE;
    }
    foreach ($finder_files as $file) {
      $this->templateFilePath = $file->getPath() . '/' . $file->getFilename();
      return $this->templateFilePath;
    }
  }

}
